document.addEventListener("deviceReady", connectToDatabase);
document.getElementById("insertButton").addEventListener("click", saveButton);
document.getElementById("showButton").addEventListener("click", showButton);
document.getElementById("rescueButton").addEventListener("click", rescueButton);
 var name = 0;
 var avail = 0;
var db = null;
function showButton() {
    //var name = document.getElementById("nameBox").value;
    alert("you pressed show");
    db.transaction(
        function(tx){
            tx.executeSql( "SELECT * FROM heroData",
            [],
            displayResults,
            onError )
        },
        onError,
        onReadyTransaction
    )
}
function rescueButton() {
  alert("Vibrate TEST");
  navigator.vibrate(3000);
}


function saveButton() {
    console.log("you pressed save");
    //var dept = document.getElementById("deptBox").value;
    name = document.getElementById("nameBox").value;
    avail = document.getElementById("availableBox").value;
    alert(name + avail);
    db.transaction(
        function(tx){
            // Execute the SQL via a usually anonymous function 
            // tx.executeSql( SQL string, arrary of arguments, success callback function, failure callback function)
            // To keep it simple I've added to functions below called onSuccessExecuteSql() and onFailureExecuteSql()
            // to be used in the callbacks
            tx.executeSql(
                "INSERT INTO heroData (name, dept) VALUES(?,?)",
                [name,avail],
                onSuccessExecuteSql,
                onError
            )
        },
        onError,
        onReadyTransaction
    )
    
}
function displayResults( tx, results ){
    var hire = "yes";
    if(results.rows.length == 0) {
            alert("No records found");
            return false;
        }
 
        var row = "";
        for(var i=0; i<results.rows.length; i++) {
            if (results.rows.item(i).isAvailable == 1){
            hire = "yes";
        }
        else{
            hire = "no";

        }
      document.getElementById("all").innerHTML +=
          "<p> Name: "
        +   results.rows.item(i).name
        + "<br>"
        + "Available to Hire: "
        +   hire
        + "</p>";
     
        }
 
    }


function onReadyTransaction(){
  console.log( 'Transaction completed' )
}
function onSuccessExecuteSql( tx, results ){
  console.log( 'Execute SQL completed' )
}
function onError( err ){
  console.log( err )
}



function connectToDatabase() {
  console.log("device is ready - connecting to database");
 

  // 2. open the database. The code is depends on your platform!
  if (window.cordova.platformId === 'browser') {
    console.log("browser detected...");
    // For browsers, use this syntax:
    //  (nameOfDb, version number, description, db size)
    // By default, set version to 1.0, and size to 2MB
    db = window.openDatabase("hero", "1.0", "Database for heroes app", 3*1024*1024);
  }
  else {
    alert("mobile device detected");
    console.log("mobile device detected!");
    var databaseDetails = {"name":"hero.db", "location":"default"}
    db = window.sqlitePlugin.openDatabase(databaseDetails);
    console.log("done opening db");
  }

  if (!db) {
    alert("databse not opened!");
    return false;
  }

db.transaction(
        function(tx){
            // Execute the SQL via a usually anonymous function 
            // tx.executeSql( SQL string, arrary of arguments, success callback function, failure callback function)
            // To keep it simple I've added to functions below called onSuccessExecuteSql() and onFailureExecuteSql()
            // to be used in the callbacks
            tx.executeSql(
                "CREATE TABLE IF NOT EXISTS heroData (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, isAvailable INTEGER)",
                [],
                onSuccessExecuteSql,
                onError
            )
        },
        onError,
        onReadyTransaction
    )

}

